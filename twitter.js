var twitter = require('ntwitter');
var credentials = require('./credentials.js');

var t = new twitter({
	consumer_key: credentials.consumer_key,
	consumer_secret: credentials.consumer_secret,
	access_token_key: credentials.access_token_key,
	access_token_secret: credentials.access_token_secret
});

var screenNameToFollow = ['aaojo', 'khairinofb', 'andreputramu', 'prizaanggara'];

t.showUser(
	screenNameToFollow,
	function(err, data) {
		var idToFollow = [];
		for (i = 0, len = data.length; i < len; i++) {
			idToFollow[i] = data[i].id;
		}
		console.log(idToFollow);
		t.stream(
			'statuses/filter',
			{follow: idToFollow},
			function(data) {
				data.on('data', function(tweet) {
					// Only tweet original tweet, not retweeted tweet
					if (tweet.retweeted_status == null) {
						t.retweetStatus(tweet.id_str, function(err, data) {
							console.log('Retweeted @' + tweet.user.screen_name + ' (' + tweet.id_str + '): ' + tweet.text);
						});
					}
				});
			}
		);
	}
);
